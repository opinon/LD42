﻿using UnityEngine;


[RequireComponent(typeof(CharacterController))]
public class PlayerControls : MonoBehaviour
{
    private CharacterController controller;

    public float CamSensi = 10f;
    public float Speed = 4f;

    private Camera camera;
    private float cameraRotation;
    public float cameraLimitMin = -30;
    public float cameraLimitMax = 40;

    // Use this for initialization
    void Start()
    {
        this.controller = this.GetComponent<CharacterController>();
        Screen.lockCursor = true;

        camera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        // Gets input axes
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        float view_y = -Input.GetAxis("Mouse Y");
        float view_x = Input.GetAxis("Mouse X");

        // Applies rotation on X axis (camera follows body)
        transform.Rotate(transform.up, view_x * Time.deltaTime * CamSensi);

        // Applies rotation on Y axis (camera only)
        cameraRotation += view_y;
        cameraRotation = Mathf.Clamp(cameraRotation, cameraLimitMin, cameraLimitMax);
        camera.transform.localRotation = Quaternion.Euler(cameraRotation, 0, 0);

        // Handles movement
        Vector3 move = transform.forward * vertical * Speed + transform.right * horizontal * Speed;
        controller.SimpleMove(move);

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Screen.lockCursor = false;
        }
    }
}