﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPicker : MonoBehaviour
{
    /// Distance from the item to the player's face
    public float distance = 2f;

    private Boolean hasPicked;
    private Pickable pickedItem;
    private Camera camera;

    // Use this for initialization
    void Start()
    {
        hasPicked = false;
        camera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        // Modifie la position du picker
        transform.position = camera.transform.position + camera.transform.forward * distance;

        if (Input.GetButtonDown("Action"))
        {
            if (this.hasPicked)
            {
                this.Unpick();
                return;
            }

            // Tries to pick an object
            RaycastHit hit;
            if (Physics.Raycast(this.camera.transform.position, this.camera.transform.forward, out hit))
            {
                Pickable p = hit.collider.gameObject.GetComponent<Pickable>();
                if (p != null)
                {
                    this.Pick(p);
                }
            }
        }
    }

    /// <summary>
    /// Picks up the targeted item
    /// </summary>
    /// <param name="p">The object's Pickable component</param>
    void Pick(Pickable p)
    {
        if (p.TryPick(this.GetComponent<Rigidbody>()))
        {
            this.hasPicked = true;
            this.pickedItem = p;
        }
    }

    /// <summary>
    /// Leaves the picked item
    /// </summary>
    void Unpick()
    {
        this.pickedItem.Unpick();
        this.hasPicked = false;
    }
}