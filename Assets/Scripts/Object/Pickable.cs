﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickable : MonoBehaviour
{
    public Boolean pickable;
    private Boolean isPicked;

    private HingeJoint joint;

    // Use this for initialization
    void Start()
    {
        this.isPicked = false;
    }

    // Update is called once per frame
    void Update()
    {
    }

    /// <summary>
    /// Tries to pickup this object
    /// </summary>
    /// <param name="rb">The ItemPicker's RigidBody</param>
    /// <returns>True if succeded, false otherwise</returns>
    public bool TryPick(Rigidbody rb)
    {
        if (this.pickable)
        {
            this.Pick(rb);
            return true;
        }

        return false;
    }

    /// <summary>
    ///  Called when this object is picked up by an ItemPicker
    /// </summary>
    /// <param name="rb">The ItemPicker's Rigidobdy</param>
    private void Pick(Rigidbody rb)
    {
        // Creates the joint and associates the target's rb
        this.joint = this.gameObject.AddComponent<HingeJoint>();
        this.joint.connectedBody = rb;

        // Setups the spring
        this.joint.useSpring = true;
        JointSpring spring = new JointSpring();
        spring.spring = 30;
        this.joint.spring = spring;
    }

    /// <summary>
    /// Called when this item gets unpicked
    /// </summary>
    public void Unpick()
    {
        this.joint.connectedBody = null;
        Destroy(this.joint);
    }
}