Puzzle game "Escape Game"

But: s'échapper du niveau dans lequel on est. Recommencer l'opération un nombre
défini de fois pour gagner.

Génération procédurale de niveau!
Suit le fonctionnement de Binding of Isaac (à base de pièces pour construire un niveau)

Le niveau est généré sous forme de tuiles. Toutes les X secondes, une tuile est "attaquée" par une IA maléfique/un robot méchant/YMMV. Une tuile attaquée est susceptible d'être détruite sans action de la part du joueur, réduisant ainsi l'espace de jeu et empêchant le joueur d'accomplir son but.

Il est possible de sauver une tile un nombre limité de fois (une ou deux ?). La destruction de cette dernière est alors annulée. Pour ce faire, on sort le téléphone et on choisit quelle tile sauver.

Une tuile attaquée est visible car elle passe en lumière rouge. Son attaque est signalé par un bruit dans les hauts parleurs.

Deux types de tuile : 
* Tuiles qui contiennent des éléments de scénario important (boutons à appuyer, objets à récupérer, pousser)
* Tuiles "de passage" qui seront les premières à être supprimées.
